extern crate ansi_term;

use ansi_term::Style;
use ansi_term::Colour::{Blue, Cyan, Yellow};


fn main() {
    println!(
        "Yellow on blue: {}",
        Style::new().on(Blue).fg(Yellow).paint("yow!")
    );
    println!(
        "Also yellow on blue: {}",
        Cyan.on(Blue).fg(Yellow).paint("zow!")
    );
}
